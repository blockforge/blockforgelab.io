---
title: "Download BlockForge"
date: 2018-01-21
---

Our latest release is version 0.0.4, released Febuary 3, 2018.

<a href="https://dl.bintray.com/jgillich/blockforge/blockforge-0.0.4-windows-amd64.zip" class="button is-primary">
  <i class="fa fa-windows"></i>&nbsp;&nbsp;Download for Windows
</a>
<a href="https://dl.bintray.com/jgillich/blockforge/blockforge-0.0.4-linux-amd64.tar.bz2" class="button is-primary">
  <i class="fa fa-linux"></i>&nbsp;&nbsp;Download for Linux
</a>

You can find older releases <a href="https://dl.bintray.com/jgillich/blockforge/">here</a>.

### Getting Started on Windows

To launch the graphical user interface, simply double click the executable.
If you want access the command line interface, launch `blockforge.exe --help` for more details.

### Getting Started on Linux

On Linux, first make sure you have the following dependencies installed: hwloc, ocl-icd, webkit2gtk and a OpenCL driver from your graphics card vendor.

On Debian and Ubuntu, the first three are installed via:

```sh
sudo apt install libhwloc5 ocl-icd-libopencl1 libwebkit2gtk-4.0-37
```

The OpenCL driver is included with the [amdgpu-pro driver](http://support.amd.com/en-us/kb-articles/Pages/AMD-Radeon-GPU-PRO-Linux-Beta-Driver%E2%80%93Release-Notes.aspx), for Intel it is available [from here](https://software.intel.com/en-us/articles/opencl-drivers).

After installing all of these, run `blockforge gui` to launch the graphical user interface, or `blockforge --help`
for command line usage.

### Need Help?

If you need help, please create an issue on [our GitLab page](https://gitlab.com/blockforge/blockforge)
or post on [our subreddit](https://www.reddit.com/r/blockforge).
