---
title: "Announcing BlockForge: Mining Made Easy"
date: 2018-01-21
---
We found that there is a big demand for easy to use mining software, but only shady proprietary
options exist. We want to change that. Introducing BlockForge, a next generation miner that ticks all the boxes:

* Easy to use
* Multiple algorithms
* CPU and GPU mining
* Open source

Here's how it looks:

![](/images/2018/blockforge.jpg)

Or do you prefer the command line?

```sh
$ blockforge miner --init
Wrote config file to '~/.config/blockforge/config.yml'
$ blockforge miner
19:55:03	info	miner started
19:56:03	info	job difficulty 7500
19:56:03	info	CPU 0: 75.32 H/s
```

BlockForge is open source under the MIT license, you can check out our code
[on GitLab](https://gitlab.com/blockforge/blockforge). We use core crypto code derived from Monero
and other projects, everything else was written from scratch to fit our needs.

Because you have to start somewhere, we do with Monero and other coins using the Cryptonight algorithm.
Cryptonight can be efficiently computed on both CPUs and GPUs and does not require special hardware,
which makes it a great default option. Support for more algorithms will be added over time.

GPU mining is implemented via the OpenCL API, which means we support graphics processors from AMD
and Intel. While NVIDIA does have limited OpenCL support, we were not able to test it due to a
lack of hardware. CUDA support is planned for the future.

Along with this announcement, we are releasing our first alpha version, available for Windows and Linux.
Get it from the [download page]({{< ref "download.md" >}}).

Note that this is merely a preview, performance is expected to be worse than most other miners since
we still have some optimization work to do. And there are probably lots of bugs, too. If you find
any, please report them by creating an issue on [our GitLab](https://gitlab.com/blockforge/blockforge)
page (if possible, set the `--debug` flag on the command line and attach the output).

For general discussion and feedback, we also have [a subreddit](https://www.reddit.com/r/blockforge/).

We have big plans for BlockForge, but to make it happen, we need your help in any of the following
areas:

* **Programming**: Most of our code is written in Go, the crypto is implemented in C and the graphical
user interface uses JavaScript and HTML. If you're interested in contributing in any of these areas,
please get in touch.

* **Donations**: We need to purchase a dual GPU mining rig for OpenCL/CUDA development and testing.
This will cost us approximately $500. If you can spare some Monero, please send them to:

```sh
46DTAEGoGgc575EK7rLmPZFgbXTXjNzqrT4fjtCxBFZSQr5ScJFHyEScZ8WaPCEsedEFFLma6tpLwdCuyqe6UYpzK1h3TBr
```

Thanks!
